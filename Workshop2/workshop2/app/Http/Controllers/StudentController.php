<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student; 
use App\Http\Resources\StudentResource; 

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $data = Student::select('*')->get();
        return response()->json($data, 200); 
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Student::create($request->all()); 
        $data = Student::latest('id')->first(); 

        $data = new Student(); 
        $data->firstname = $request->input('firstname');
        $data->lastname = $request->input('lastname');
        $data->email = $request->input('email');
        $data->address = $request->input('address');

        $data->save();

        return response()->json($data,201)
            ->header(['Location'=>'http://localhost:8000/api/students/'.$data->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = new StudentResource(Student::find($id));
        return response()->json($data, 200); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $res = Student::findOrFail($id);
        $res->update($request->all());
        
        return response()->json(['status'=>'ok'], 200);
        /*
        $data = new Student(); 
        $data->firstname = $request->input('firstname');
        $data->lastname = $request->input('lastname');
        $data->email = $request->input('email');
        $data->address = $request->input('address');

        $data->update();*/
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Student::destroy($id);
        if($data > 0){
            return response('Deleted', 204)
                  ->header('Content-Type', 'text/plain');
        }
        return response('Error', 409)
                  ->header('Content-Type', 'text/plain');
    }
}
