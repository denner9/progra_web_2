$(document).ready(function () {
    var request = new XMLHttpRequest();
    var url = "https://ghibliapi.herokuapp.com/";
    var endpoint = "films/";
    var movieComponent = $('#movies');
    function getData() {

        request.open("GET", url + endpoint, true);

        request.onload = function () {
            // Begin accessing JSON data here
            var data = JSON.parse(this.response);
            
            movieComponent.empty();
            data.forEach(movie => {

                // Log each movie's title
                console.log(movie.title);
                var dataString = `<p> ${movie.title} | ${movie.id}</p> <br>`;
                movieComponent.append(dataString);
            });

        }

        request.send();

    }

    $('#btn').click(function () {
        getData();
    });


    $('#btnSearch').click(function () {
        var id = $('#txt').val();

        request.open("GET", url + endpoint + id, true);

        request.onload = function () {
            // Begin accessing JSON data here
            var data = JSON.parse(this.response);

            var divHtml = $('#movies');
            movieComponent.empty();
            console.log(data.title);
            var dataString = `<p> ${data.title} | ${data.id}</p> <br>`;
            movieComponent.append(dataString);

        }

        request.send();
    });


});



