import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAddserviceComponent } from './admin-addservice.component';

describe('AdminAddserviceComponent', () => {
  let component: AdminAddserviceComponent;
  let fixture: ComponentFixture<AdminAddserviceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminAddserviceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAddserviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
