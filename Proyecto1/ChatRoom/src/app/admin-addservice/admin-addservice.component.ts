import { Component, OnInit } from '@angular/core';
import { AdminConsoleService } from '../Services/admin-console.service';
import { TChannel } from '../Classes/tchannel';

@Component({
  selector: 'app-admin-addservice',
  templateUrl: './admin-addservice.component.html',
  styleUrls: ['./admin-addservice.component.scss']
})
export class AdminAddserviceComponent implements OnInit {

  private channel: TChannel;
  private inserted: boolean;

  constructor(private adminConsoleService: AdminConsoleService) {

  }

  ngOnInit() {
    this.channel = new TChannel();
    this.inserted = false;
  }

  onSubmit() {
    this.adminConsoleService.addChannel(this.channel).then(() => {
      this.adminConsoleService.getChannels();
      this.inserted = true;
    }).catch(err => {
      alert("The Unique Name already exists, please choose another one."); 
      console.log("error: " + err);
    });

  }

}
