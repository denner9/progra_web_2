export class TMessage {

    sid: string;
    accountSid: string;
    to: string;
    from: string;
    body: string;
    url: string;
    created: Date

    constructor(){
        
    }
}