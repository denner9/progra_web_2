import { TChannel } from './tchannel';

export class TMember{
    sid: string; 
    name: string; 
    url: string; 
    role_sid: string;
    channel: TChannel; 
    
    constructor(){

    }
}