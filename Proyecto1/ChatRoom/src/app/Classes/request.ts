import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

export class Request {

    private params = {
        'username': "AC3f13829fca335917ccb351264be8378c",
        'password': "ef73ee9e89e9292ae74b6f27ab38fc3d"
    };
    private url: string;
    private httpOptions = {
        headers: new HttpHeaders({
            'Authorization': "Basic " + btoa(this.params.username + ":" + this.params.password)
        })
    };

    constructor(private http: HttpClient) {
        this.url = 'https://chat.twilio.com/v2/Services/';
    }

    //*********************************************************


    /**
     * HttpGet Method
     * @param uri URI to make the request.
     */
    public get(uri: string) {
        this.httpOptions.headers.append('Content-Type', 'application/json');
        return this.http.get(this.url + uri, this.httpOptions);
    }

    /**
     * HttpDelete Method
     * @param uri URI to make the request.
     */
    public delete(uri: string) {
        return this.http.delete(this.url + uri, this.httpOptions);
    }

    /**
     * HttpPost Method 
     * @param uri URI to make the request.
     * @param param Params for the request. 
     */
    public post(uri: string, param: any) {
        this.httpOptions.headers.append('Content-Type', 'application/x-www-form-urlencoded');
        let body = new HttpParams();
        
        for (const key in param) {
            if (param.hasOwnProperty(key)) {
                body = body.append(key, param[key]);
            }
        }
        return this.http.post(this.url + uri, body, this.httpOptions);
    }
}
