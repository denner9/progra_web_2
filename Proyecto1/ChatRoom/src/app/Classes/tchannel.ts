export class TChannel {
    uname: string;
    members: number;
    name: string;
    url: string;
    sid: string;
    links: {
        webhooks: string,
        messages: string,
        invites: string,
        members: string,
        last_message: string
    };

    constructor() {
        this.uname = "";
        this.members = -1;
        this.name = "";
        this.url = null;
        this.sid = null;
        this.links = null; 
    }
}




