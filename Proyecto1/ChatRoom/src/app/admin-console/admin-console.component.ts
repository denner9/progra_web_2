import { Component, OnInit } from '@angular/core';
import { AdminConsoleService } from '../Services/admin-console.service';
import {Router} from '@angular/router'; 

@Component({
  selector: 'app-admin-console',
  templateUrl: './admin-console.component.html',
  styleUrls: ['./admin-console.component.scss']
})
export class AdminConsoleComponent implements OnInit {
 
  constructor(adminConsoleService: AdminConsoleService, private router : Router) {
    adminConsoleService.getService();
    
   }

  ngOnInit() {
    
  }

  private goToHomePage(){
    sessionStorage.clear();
    this.router.navigate(['/home']);
  }
}
