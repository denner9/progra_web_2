import { Component, OnInit, destroyPlatform } from '@angular/core';
import { ChannelManagementService } from '../Services/channel-management.service';
import { AdminConsoleService } from '../Services/admin-console.service';
import { TMember } from '../Classes/tmember';
import { TMessage } from '../Classes/tmessage';
import { Router } from '@angular/router';
import { routerNgProbeToken } from '@angular/router/src/router_module';

@Component({
  selector: 'app-chatroom',
  templateUrl: './chatroom.component.html',
  styleUrls: ['./chatroom.component.scss']
})
export class ChatroomComponent implements OnInit {

  private user: TMember;
  private username: string;
  private isLogged: boolean = false;
  private isError: boolean = false;
  private err: any = { error: { message: "" } };
  private message: string;
  private messages: Array<TMessage>;
  private members: Array<TMember>;
  private showMembers: boolean;


  constructor(private service: AdminConsoleService, private channelService: ChannelManagementService, private router: Router) {
    let dataStored = service.getStoredService();
    let dataStoredChannel = JSON.parse(sessionStorage.getItem('channelInUse'));

    if (dataStored) {
      service.service = dataStored;
    }

    if (dataStoredChannel) {
      channelService.channel = dataStoredChannel;
    }
  }

  ngOnInit() {
    this.user = JSON.parse(sessionStorage.getItem('member-' + this.channelService.channel.uname));
    if (this.user != null) {
      this.isLogged = true;
      this.getAutomaticallyData(); 
      //Get all the messages
    }
    this.username = "";
    this.message = "";
    this.showMembers = false;
  }

  /**
   * Method to automatically get the data and update
   */
  private getAutomaticallyData() {
    setInterval(() => {
      this.getMessages();
      this.getMembers();
    }, 500);
  }
  /**
   * Make the request to the local service to get the messages
   */
  private getMessages() {
    this.channelService.getChannelMessages(this.service.service, this.channelService.channel)
      .then((data: Array<TMessage>) => {

        this.messages = data;
      }).catch(err => console.log(err));
  }
  /**
   * On submit the form to get logged
   */
  onSubmit() {
    if (this.username.trim() != "") {
      this.channelService.addMemberToChannel(this.service.service, this.channelService.channel, this.username)
        .then((data: TMember) => {
          this.user = data;
          this.isLogged = true;
          sessionStorage.setItem('member-' + this.channelService.channel.uname, JSON.stringify(this.user));
          this.getAutomaticallyData();
        }).catch(err => {
          this.err = err;
          console.log(err); 
          if(err.error.message.includes("was not found")){
            err.error.message = "Sorry the channel was deleted. Please try with another one.";
          }
          this.isError = true;
        });
      return;
    }

    this.isError = true;
    this.err.error.message = "Please write an username. Empty username space";

  }

  /**
   * Send the message
   */
  private sendMsg() {
    let msg = new TMessage();
    msg.body = this.message;
    msg.from = this.user.name;
    this.message = "";
    this.channelService.sendMessage(this.service.service, this.channelService.channel, msg)
    .catch(() => {
      confirm("There was an error with the channel. IT Support it is working on it. Continue chating with people in the another chatroom. "); 
      sessionStorage.setItem('member-' + this.channelService.channel.uname, JSON.stringify(null));
      this.router.navigate(['home']); 
      destroyPlatform();
    });
  }

  /**
   * Get all the members to dysplay it
   */
  private getMembers() {
    this.channelService.getAllChannelMembers(this.service.service, this.channelService.channel)
      .then((members: Array<TMember>) => {
        this.members = members;
      });
  }

  /**
   * The user can logout
   */
  private logout() {
    if (confirm("Do you really want to logout?")) {
      this.channelService.deleteMember(this.service.service, this.channelService.channel, this.user)
        .then(() => {
          sessionStorage.setItem('member-' + this.channelService.channel.uname, JSON.stringify(null));
          this.router.navigate(['/home']);
        }).catch(err => console.log(err));
    }

  }

}
