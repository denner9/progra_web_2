import { Component, OnInit } from '@angular/core';
import { AdminConsoleService } from '../Services/admin-console.service';
import { TChannel } from '../Classes/tchannel';
import { ChannelManagementService } from '../Services/channel-management.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-channel-cards',
  templateUrl: './channel-cards.component.html',
  styleUrls: ['./channel-cards.component.scss']
})
export class ChannelCardsComponent implements OnInit {

  dataStored: any;

  constructor(private service: AdminConsoleService, private channelService: ChannelManagementService,
    private router: Router) {
    this.dataStored = service.getStoredService();
    if (this.dataStored) {
      service.service = this.dataStored;
    }

  }

  ngOnInit() {
    if (this.service.service.sid) {
      this.service.getChannels();
      setInterval(() => {
        this.service.getChannels();
      }, 15000);
    }
  }
  /**
   * Moves to the channel chatroom
   * @param channel Channel where the user entered
   */
  goTo(channel: TChannel) {
    this.channelService.channel = channel;
    sessionStorage.setItem('channelInUse', JSON.stringify(channel));
    this.router.navigate(['/chatroom']);
  }


}
