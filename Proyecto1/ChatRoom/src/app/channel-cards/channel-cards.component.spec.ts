import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChannelCardsComponent } from './channel-cards.component';

describe('ChannelCardsComponent', () => {
  let component: ChannelCardsComponent;
  let fixture: ComponentFixture<ChannelCardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChannelCardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChannelCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
