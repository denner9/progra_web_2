import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; 
import { HttpClientModule } from '@angular/common/http'; 


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { AdminConsoleComponent } from './admin-console/admin-console.component';
import { AdminAddserviceComponent } from './admin-addservice/admin-addservice.component';
import { AdminChtableComponent } from './admin-chtable/admin-chtable.component';
import { ChannelCardsComponent } from './channel-cards/channel-cards.component';
import { ChatroomComponent } from './chatroom/chatroom.component'; 
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    AdminConsoleComponent,
    AdminAddserviceComponent,
    AdminChtableComponent,
    ChannelCardsComponent,
    ChatroomComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
