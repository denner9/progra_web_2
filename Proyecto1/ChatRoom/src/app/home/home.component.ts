import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'; 

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  private adminState : string; 

  constructor(private router: Router) {
    if(JSON.parse(sessionStorage.getItem('logged'))){
      this.adminState = "Console";
    }else{
      this.adminState = "Login"
    }

  }

  ngOnInit() {
  }

  private goToLoginPage(){
    let res = JSON.parse(sessionStorage.getItem('logged')); 
    if(res){
      //Logged
      this.router.navigate(['/admin-console']);
      return; 
    }
    this.router.navigate(['/login']);
  }
}
