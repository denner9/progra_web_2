import { Component, OnInit } from '@angular/core';
import { AdminConsoleService } from '../Services/admin-console.service';
import { TChannel } from '../Classes/tchannel';

@Component({
  selector: 'app-admin-chtable',
  templateUrl: './admin-chtable.component.html',
  styleUrls: ['./admin-chtable.component.scss']
})
export class AdminChtableComponent implements OnInit {

  constructor(private adminConsoleService: AdminConsoleService) { }

  ngOnInit() {

    setTimeout(() => {
      this.adminConsoleService.getChannels();
    }, 1500);
  }

  /**
   * Method to delete the channel. Makes the connection with the service that is in charge of this task. 
   * @param channel Channel to delete. 
   */
  private deleteChannel(channel: TChannel, index: number) {
    if (confirm("Do you want to delete the Channel?")) {
      this.adminConsoleService.deleteChannel(channel).then(() => {
        alert("The channel was deleted");
        this.adminConsoleService.getChannels();
      }).catch(() => alert("Problems deleting the channel."));
    }
  }

  private updateChannel(channel: TChannel, index: number) {
    if(confirm("Do you want to update the Channel?")){
      this.adminConsoleService.updateChannel(channel).then(() => {
        this.adminConsoleService.getChannels();
      });
    }

  }




}
