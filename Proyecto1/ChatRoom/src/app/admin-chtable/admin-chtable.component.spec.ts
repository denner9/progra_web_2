import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminChtableComponent } from './admin-chtable.component';

describe('AdminChtableComponent', () => {
  let component: AdminChtableComponent;
  let fixture: ComponentFixture<AdminChtableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminChtableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminChtableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
