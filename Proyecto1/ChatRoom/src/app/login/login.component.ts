import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'; 

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  
  private email : string; 
  private passw : string;
  private loginError: boolean;  
  
  constructor(private router: Router) { 
    this.email = ""; 
    this.passw = ""; 
    this.loginError = false;
  }

  ngOnInit() {
  }

  private onSubmit(){
    if(this.email == "admin@admin.com" && this.passw == "admin"){
      sessionStorage.setItem('logged', JSON.stringify(true)); 
      this.router.navigate(['/admin-console']);
      return; 
    }  
    //Error: Invalid Login
    this.loginError = true;
  }

}
