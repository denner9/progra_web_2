import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { TService } from '../Classes/tservice';
import { TChannel } from '../Classes/tchannel';
import { Request } from '../Classes/request';



@Injectable({
  providedIn: 'root'
})
export class AdminConsoleService {

  channels: Array<TChannel>;
  service: TService;
  private request: Request;

  //*********************************************************

  constructor(private http: HttpClient) {
    this.service = new TService();
    this.channels = new Array<TChannel>();
    this.request = new Request(http);
  }

  /**
   * Mehtod to manage the error messages. 
   * @param error Error getted from a response
   * @returns strings errors messages. 
   */
  private errorHandler(error: any) {

    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred.
      console.error('An error occurred:', error.error.message);
      return "An error ocurred: " + error.error.message;

    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);

      return "An server error ocurred: " + error.error;
    }

  }

  /**
   * Get the service information. Set the information in the service variable
   */
  getService() {

    this.request.get("").subscribe(data => {

      this.service = {
        sid: data['services'][0].sid,
        links: data['services'][0].links,
        url: data['services'][0].url
      };
      this.storeService(this.service); 
    },
      error => {
        throw(this.errorHandler(error));
      }
    );

  }

  /**
   * Store the service in the local storage. 
   * @param service Service to be stores
   */
  private storeService(service:any){
    localStorage.setItem('service', JSON.stringify(service)); 
  }

  /**
   * Retrieves the service from local storage
   */
  getStoredService(){
    return  JSON.parse(localStorage.getItem('service')); 
  }
  
  clearLocalStorage(){
    localStorage.clear(); 
  }
  /**
   * Method to add a new channel. 
   * @param channel Object with the channel name and the unique channel name. 
   * @returns TChannel with the information got it
   * @throws Executes a error handler method and throws a string error message.
   */
  addChannel(channel: TChannel) {

    let obj = {
      'FriendlyName': channel.name,
      'ServiceSid': this.service.sid,
      'UniqueName': channel.uname
    };

    let promise = new Promise((resolve, reject) => {

      this.request.post(this.service.sid + "/Channels/", obj)
        .subscribe((data: any) => {
          channel.members = data.members_count;
          channel.sid = data.sid;
          channel.url = data.url;
          channel.links = data.links;
          resolve(channel); //Then
        },
          error => {
            reject(this.errorHandler(error)); 
          } //Catch
        );
    });

    return promise;
  }

  /**
   * Get all the channels in ther service and update the public channels variable to be used. 
   * 
   */
  getChannels() {
    this.channels = new Array<TChannel>();

    this.request.get(this.service.sid + "/Channels/")
      .subscribe(data => { 
        data['channels'].forEach(channel => {
          let newChannel = new TChannel();
          newChannel.sid = channel.sid;
          newChannel.name = channel.friendly_name;
          newChannel.uname = channel.unique_name;
          newChannel.url = channel.url;
          newChannel.links = channel.links;
          newChannel.members = channel.members_count;

          this.channels.push(newChannel);
        });
      });
  }

  /**
   * Method to delete a channel. 
   * @param channel Channel to delete
   * @returns Boolean true in case if the channel was deleted and false if not. 
   */
  deleteChannel(channel: TChannel) {

    let promise = new Promise((resolve, reject) => {

      this.request.delete(this.service.sid + "/Channels/" + channel.sid).subscribe((data: any) => {

        if (data != null) {
          //204
          reject(false);
        }

        resolve(true);
      });
    });
    return promise;
  }

  /**
   * Method to update a channel
   * @param channel Channel to be updated
   * @returns Promise with a resolve true in case if was updated or reject false. 
   */
  updateChannel(channel: TChannel) {

    let obj = {
      'ServiceSid': this.service.sid,
      'Sid': channel.sid,
      'FriendlyName': channel.name,
      'UniqueName': channel.uname
    };

    let promise = new Promise((resolve, reject) => {

      this.request.post(this.service.sid + "/Channels/" + channel.sid + "/", obj).subscribe((data: any) => {
        channel.members = data.members_count;
        channel.sid = data.sid;
        channel.url = data.url;
        channel.links = data.links;
        resolve(channel); //Then
      },
        error => reject(this.errorHandler(error)) //Catch
      );

    });

    return promise;
  }

}
