import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Request } from '../Classes/request';
import { TService } from '../Classes/tservice';
import { TChannel } from '../Classes/tchannel';
import { TMember } from '../Classes/tmember';
import { TMessage } from '../Classes/tmessage';
import { reject } from 'q';



@Injectable({
  providedIn: 'root'
})
export class ChannelManagementService {

  private request: Request;
  channel: TChannel;

  constructor(private http: HttpClient) {
    this.request = new Request(http);
    this.channel = new TChannel();
  }

  /**
   * Method to generate an TChannel object from a http response
   * @param data Data retrieved from a http get
   * @param channel Channel in use
   * @returns TMember generated
   */
  private generateMember(data: any, channel: TChannel) {
    let member = new TMember();
    member.sid = data.sid;
    member.channel = channel;
    member.role_sid = data.role_sid;
    member.name = data.identity;
    member.url = data.url;
    return member;
  }

  /**
   * Method to add a member to a channel.
   * @param service Service using the channel
   * @param channel Channel to add the member
   * @param user Name choosed by the user.
   * @returns Promise: resolve(TMember) or reject(Error message)
   */
  addMemberToChannel(service: TService, channel: TChannel, user: string) {
    let params = {
      'ServiceSid': service.sid,
      'ChannelSid': channel.sid,
      'Identity': user
    };

    let promise = new Promise((resolve, reject) => {

      this.request.post(service.sid + "/Channels/" + channel.sid + "/Members", params)
        .subscribe((data: any) => {
          let member = this.generateMember(data, channel);
          resolve(member);
        },
          error => reject(error));

    });

    return promise;
  }

  /**
   * Method to delete a memeber from a chat room. 
   * @param service Service in use
   * @param channel Channel where the user is getting out
   * @param user User leaving the chat room
   * @returns Promise: resolve(true:logout successful) or reject(false)
   */
  deleteMember(service: TService, channel: TChannel, user: TMember) {

    let promise = new Promise((resolve, reject) => {
      this.request.delete(service.sid + "/Channels/" + channel.sid + "/Members/" + user.name)
        .subscribe((data: any) => {

          if (data != null) {
            //204
            reject(false);
          }

          resolve(true);
        });
    });

    return promise;
  }

  /**
   * Get the channel messages. 
   * @param service Service in use
   * @param channel Specific TChannel to get the data.
   * @returns Promise: resolve(TMember) or reject(Error message)
   */
  getChannelMessages(service: TService, channel: TChannel) {
    let msgs = new Array<TMessage>();

    let promise = new Promise((resolve, reject) => {
      this.request.get(service.sid + "/Channels/" + channel.sid + "/Messages/")
        .subscribe((data: any) => {
          data['messages'].forEach((msg: any) => {
            let newMsg = new TMessage();
            newMsg.accountSid = msg.account_sid;
            newMsg.body = msg.body;
            newMsg.from = msg.from;
            newMsg.sid = msg.sid;
            newMsg.to = msg.to;
            newMsg.url = msg.url;
            newMsg.created = new Date(msg.date_created);
            msgs.push(newMsg);
          });

          resolve(msgs);
        },
          error => reject(error)
        );
    });

    return promise;
  }

  /**
   * Get all the channel members. 
   * @param service Service in use
   * @param channel Channel selected
   * @returns promise where resolve = Array<TMember>() or reject the errors.
   */
  getAllChannelMembers(service: TService, channel: TChannel) {
    let members = new Array<TMember>();
    let promise = new Promise((resolve, reject) => {
      this.request.get(service.sid + "/Channels/" + channel.sid + "/Members")
        .subscribe(data => {
          data['members'].forEach((member: any) => {
            members.push(this.generateMember(member, channel));
          });
          resolve(members);
        },
          err => reject(err));
    });

    return promise; 
  }

  /**
   * Method to send a message into a specific channel.
   * @param service Service in use
   * @param channel Channel in use
   * @param message Message will be sended
   * @returns promise where resolve = true and reject the errors. 
   */
  sendMessage(service: TService, channel: TChannel, message: TMessage) {
    let params = {
      'ServiceSid': service.sid,
      'ChannelSid': channel.sid, 
      'From': message.from,
      'Body': message.body
    }; 

    let promise = new Promise((resolve, reject) => {
      this.request.post(service.sid + "/Channels/" + channel.sid + "/Messages/", params)
      .subscribe(data => {
        resolve(true); 
      }, err => reject(err));
    });

    return promise; 
  }


}
