import { TestBed } from '@angular/core/testing';

import { ChannelManagementService } from './channel-management.service';

describe('ChannelManagementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ChannelManagementService = TestBed.get(ChannelManagementService);
    expect(service).toBeTruthy();
  });
});
